import React from "react";
import { IFormFields } from "./modal.component";
import { FormStyled, InputStyled, InputWithLabelStyled } from "./modal.styled";

interface IModalFormProps {
  fields: IFormFields;
  areFieldsValid: boolean;
  onChange: (fieldName: keyof IFormFields, value: string) => void;
  onClose: () => void;
  onSave: () => void;
}

export const ModalForm: React.FC<IModalFormProps> = ({
  fields,
  areFieldsValid,
  onChange,
  onClose,
  onSave,
}) => {
  return (
    <div
      className="modal fade show"
      tabIndex={-1}
      role="dialog"
      style={{ display: "block" }}
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title">Add new card</h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={onClose}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <FormStyled>
              <InputWithLabelStyled>
                <label>CVV</label>
                <InputStyled
                  name="cvv"
                  value={fields.cvv.value}
                  isValid={fields.cvv.isValid}
                  onChange={({ target: { name, value } }) =>
                    onChange(name as keyof IFormFields, value)
                  }
                />
              </InputWithLabelStyled>
              <InputWithLabelStyled>
                <label>Card Number</label>
                <InputStyled
                  name="cardNumber"
                  value={fields.cardNumber.value}
                  isValid={fields.cardNumber.isValid}
                  onChange={({ target: { name, value } }) =>
                    onChange(name as keyof IFormFields, value)
                  }
                />
              </InputWithLabelStyled>
              <InputWithLabelStyled>
                <label>Expired Date</label>
                <InputStyled
                  name="expiredDate"
                  value={fields.expiredDate.value}
                  isValid={fields.expiredDate.isValid}
                  onChange={({ target: { name, value } }) =>
                    onChange(name as keyof IFormFields, value)
                  }
                />
              </InputWithLabelStyled>
              <InputWithLabelStyled>
                <label>Card Holder Name</label>
                <InputStyled
                  name="cardHolderName"
                  value={fields.cardHolderName.value}
                  isValid={fields.cardHolderName.isValid}
                  onChange={({ target: { name, value } }) =>
                    onChange(name as keyof IFormFields, value)
                  }
                />
              </InputWithLabelStyled>
            </FormStyled>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
              onClick={onClose}
            >
              Close
            </button>
            <button
              type="button"
              className="btn btn-primary"
              onClick={onSave}
              disabled={!areFieldsValid}
            >
              Save changes
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
