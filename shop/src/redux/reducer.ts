import { createSlice } from '@reduxjs/toolkit';
import { ICreditCard } from '../interfaces/creditCard.interface';
import { IProduct } from '../interfaces/product.interface';

interface IState{
    main: IMainState
}

interface IMainState {
    productsInTheBasket: IProduct[];
    allProducts: IProduct[];
    creditCards: ICreditCard[];
    selectedCreditCardId: string,
    numberOfItemsInTheBasket: number
}
interface IAction<T> {
    type: string;
    payload:T
}

const initialState: IMainState = {
    productsInTheBasket: [],
    allProducts: [],
    creditCards: [],
    selectedCreditCardId: "",
    numberOfItemsInTheBasket: 0
};

export const mainSlice = createSlice({
    name: 'main',
    initialState,
    reducers: {
        setProductsInTheBasket: (state: IMainState, action: IAction<IProduct[]>) => {
            state.productsInTheBasket = action.payload
            state.numberOfItemsInTheBasket =  action.payload.length
        },
        setAllProducts: (state: IMainState, action: IAction<IProduct[]>) => {
            state.allProducts = action.payload
        },
        setCreditCards: (state:IMainState, action:IAction<ICreditCard[]>) => {
            state.creditCards = action.payload
        },
        setSelectedCreditCardId: (state:IMainState, action:IAction<string>) => {
            state.selectedCreditCardId = action.payload
        },
        addNewCreditCard: (state:IMainState, action:IAction<ICreditCard>) => {
            state.creditCards = [
                ...state.creditCards,
                action.payload
            ]
        },
        clearNumberOfItemsInBasket: (state:IMainState) => {
            state.numberOfItemsInTheBasket = 0
        }
    },
});

export const {
    setCreditCards,
    setProductsInTheBasket,
    setSelectedCreditCardId,
    addNewCreditCard,
    clearNumberOfItemsInBasket,
    setAllProducts } = mainSlice.actions;

export const creditCards = (state: IState):ICreditCard[] => state.main.creditCards;
export const productsInTheBasket = (state: IState) => state.main.productsInTheBasket;
export const allProducts = (state: IState) => state.main.allProducts;
export const numberOfProductsInTheBasket = (state: IState) => state.main.numberOfItemsInTheBasket;
export const selectedCreditCardId = (state: IState) => state.main.selectedCreditCardId;
export const selectedCreditCard = (state: IState): ICreditCard | undefined => state.main.creditCards.find(c => c.id === state.main.selectedCreditCardId);


export default mainSlice.reducer;
