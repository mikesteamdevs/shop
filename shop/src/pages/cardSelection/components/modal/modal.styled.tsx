import styled, { css } from "styled-components";
import { flexFn } from "../../../../styles/common.styled";
import { IValidation } from "./modal.component";

interface IInputStyled extends IValidation {}

export const FormStyled = styled.div`
  ${flexFn("center", "start", "column")}
`;

const InvalidInputStyled = css`
  border-color: red;
`;

export const InputStyled = styled.input<IInputStyled>`
  margin-bottom: 1rem;
  width: 100%;
  outline: none;
  ${({ isValid }) => !isValid && InvalidInputStyled};
`;
export const InputWithLabelStyled = styled.div`
  width: 100%;
  ${flexFn("flex-start")} > * {
    width: 100%;
  }
`;

export const ModalFormWrapperStyled = styled.div`
  .modal {
    margin: 0;
    background-color: #bdbbbba8;
    height: 100%;

    .modal-dialog {
      margin-top: 50vh;
      transform: translate(0%, -50%);
    }
  }
`;
