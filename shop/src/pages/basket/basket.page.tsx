import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ButtonsSection } from "../../components/buttonsSection/buttonsSection.components";
import { Card } from "../../components/card/card.component";
import { productsInTheBasket } from "../../redux/reducer";
import { sumProducts } from "../../utils/utils";
import { BasketPageStyled, CardContentStyled } from "./basket.styled";

export const Basket = () => {
  const products = useSelector(productsInTheBasket);
  const sum = useMemo((): number => sumProducts(products), [products]);
  return (
    <BasketPageStyled>
      {products.map(({ name, price }) => (
        <Card key={name}>
          <CardContentStyled>
            <span>{name}</span>
            <span>{price} PLN</span>
          </CardContentStyled>
        </Card>
      ))}
      <Card theme="dark">
        <CardContentStyled>
          <span>Sum</span>
          <span>{sum} PLN</span>
        </CardContentStyled>
      </Card>
      <ButtonsSection
        nextComponent={
          <Link to="/select-card">
            <button className="btn btn-primary">Go to payment</button>
          </Link>
        }
        backComponent={
          <Link to="/">
            <button className="btn btn-primary">Go back</button>
          </Link>
        }
      />
    </BasketPageStyled>
  );
};
