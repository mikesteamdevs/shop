import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { ButtonsSection } from "../../components/buttonsSection/buttonsSection.components";
import { Modal } from "./components/modal/modal.component";
import {
  creditCards,
  selectedCreditCardId,
  setCreditCards,
  setSelectedCreditCardId,
  addNewCreditCard,
} from "../../redux/reducer";
import { CreditCardsService } from "../../services/creditCards.service";
import { CardSelectionPageStyled } from "./cadSelection.styled";
import { Slider } from "./components/slider/slider.component";
import { ICreditCard } from "../../interfaces/creditCard.interface";

export const CardSelection = () => {
  const dispatch = useDispatch();
  const cards = useSelector(creditCards);
  const selectedCardId = useSelector(selectedCreditCardId);
  const [isModalOpen, setIsModalOpen] = useState(false);

  useEffect(() => {
    const getAllProducts = async () => {
      const data = await CreditCardsService.getAllCreditCards();
      const firstCardId = data.length ? data[0].id : "";
      dispatch(setCreditCards(data ? data : []));
      dispatch(setSelectedCreditCardId(firstCardId));
    };
    getAllProducts();
  }, [dispatch]);

  const onNext = () => {
    if (!cards.length) return null;
    const numberOfItems = cards.length - 1;
    const index = cards.findIndex(({ id }) => id === selectedCardId);
    if (index >= 0) {
      const nextIndex = numberOfItems >= index + 1 ? index + 1 : 0;
      dispatch(setSelectedCreditCardId(cards[nextIndex].id));
    }
  };

  const onPrev = () => {
    if (!cards.length) return null;
    const numberOfItems = cards.length - 1;
    const index = cards.findIndex(({ id }) => id === selectedCardId);
    if (index >= 0) {
      const nextIndex = 0 <= index - 1 ? index - 1 : numberOfItems;
      dispatch(setSelectedCreditCardId(cards[nextIndex].id));
    }
  };

  const toggleModal = () => {
    setIsModalOpen((prev) => !prev);
  };

  const onSubmit = (card: ICreditCard) => {
    dispatch(addNewCreditCard(card));
    dispatch(setSelectedCreditCardId(card.id));
    toggleModal();
  };

  return (
    <CardSelectionPageStyled>
      <Slider
        cards={cards}
        activeCardId={selectedCardId}
        onNext={onNext}
        onPrev={onPrev}
      />
      <ButtonsSection
        nextComponent={
          <button className="btn btn-info" onClick={toggleModal}>
            Add new card
          </button>
        }
      />
      <ButtonsSection
        nextComponent={
          <Link to="/transaction-confirmation">
            <button className="btn btn-primary">Finalize payment</button>
          </Link>
        }
        backComponent={
          <Link to="/basket">
            <button className="btn btn-primary">Go back</button>
          </Link>
        }
      />
      {isModalOpen && <Modal onClose={toggleModal} onSave={onSubmit} />}
    </CardSelectionPageStyled>
  );
};
