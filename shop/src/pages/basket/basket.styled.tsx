import styled from "styled-components";
import { flexFn, marginFn } from "../../styles/common.styled";

export const CardContentStyled = styled.div`
  ${flexFn("space-between", "center")}
`;
export const BasketPageStyled = styled.div`
  ${marginFn()}
`;
