import styled from "styled-components";
import { COLORS } from "../../styles/colors";
import { flexFn, marginFn } from "../../styles/common.styled";

export const TransactionConfirmationPageStyled = styled.div`
  display: grid;
  grid-template-rows: repeat(3, auto);
  > * {
    ${marginFn()}
  }
`;
export const SuccessTitleStyled = styled.div`
  height: 10rem;
  ${flexFn("center", "center", "column")}

  > i {
    font-size: 2rem;
    height: 5rem;
    width: 5rem;
    background-color: ${COLORS.green};
    border-radius: 100%;
    color: ${COLORS.white};
    ${flexFn("center", "center", "column")}
  }
`;

export const CreditCardInfoStyled = styled.div`
  margin-top: 1rem;
`;
