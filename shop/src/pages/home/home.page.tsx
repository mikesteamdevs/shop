import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ProductsService } from "../../services/products.service";
import { Product } from "./components/product/product.component";

import {
  allProducts,
  setAllProducts,
  setProductsInTheBasket,
} from "../../redux/reducer";
import { HomePageStyled } from "./home.styled";

export const Home = () => {
  const products = useSelector(allProducts);
  const dispatch = useDispatch();

  useEffect(() => {
    const getAllProducts = async () => {
      const data = await ProductsService.getAllProducts();
      dispatch(setAllProducts(data));
    };

    const getProductsInBasket = async () => {
      const data = await ProductsService.getProductsInBasket();
      dispatch(setProductsInTheBasket(data));
    };
    getProductsInBasket();
    getAllProducts();
  }, [dispatch]);

  return (
    <HomePageStyled>
      {products &&
        products.map((product) => (
          <Product key={product.id} product={product} />
        ))}
    </HomePageStyled>
  );
};
