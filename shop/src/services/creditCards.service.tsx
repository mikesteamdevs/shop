import creditCards from "../data/creditCards.json";
import { ICreditCard } from "../interfaces/creditCard.interface";

type CreditCardsResponse = ICreditCard[];

export class CreditCardsService {
  public static getAllCreditCards = (): Promise<CreditCardsResponse> => {
    return new Promise((resolve, _reject) => resolve(creditCards));
  };
}
