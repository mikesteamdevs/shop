import React from "react";
import { ICreditCard } from "../../../../interfaces/creditCard.interface";
import { getMaker } from "../../../../utils/utils";
import {
  CardItemStyled,
  CardNumberItemStyled,
  CreditCardStyled,
  DateCVVStyled,
  LogoStyled,
} from "./creditCard.styled";

interface ICreditCardProps {
  card: ICreditCard;
}

export const CreditCard: React.FC<ICreditCardProps> = ({
  card: { cardHolderName, cardNumber, cvv, expirationDate },
}) => {
  const maker = getMaker(cardNumber);
  return (
    <CreditCardStyled theme={maker}>
      <LogoStyled>
        <p>{getMaker(cardNumber)}</p>
      </LogoStyled>

      <CardNumberItemStyled>
        <p>{cardNumber}</p>
      </CardNumberItemStyled>
      <DateCVVStyled>
        <p>Expiration date {expirationDate}</p>
        <p>CVV: {cvv}</p>
      </DateCVVStyled>
      <CardItemStyled>
        <p>{cardHolderName}</p>
      </CardItemStyled>
    </CreditCardStyled>
  );
};
