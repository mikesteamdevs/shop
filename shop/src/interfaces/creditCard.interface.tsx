export interface ICreditCard {
  id: string;
  cardNumber: string;
  expirationDate: string;
  cvv: string;
  cardHolderName: string;
}
