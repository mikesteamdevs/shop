import styled from "styled-components";
import { COLORS } from "../../../../styles/colors";
import { flexFn } from "../../../../styles/common.styled";

export const SliderItemWrapperStyled = styled.div`
  width: 100%;
  height: 18rem;
  background-color: ${COLORS.sliderBg};
  ${flexFn()}
`;
