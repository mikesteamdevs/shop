import React from "react";

type ThemeType = "dark" | "light";

interface ICardProps {
  theme?: ThemeType;
}

export const Card: React.FC<ICardProps> = ({ theme = "light", children }) => {
  const themeClass =
    theme === "light" ? "text-dark bg-light" : "bg-secondary text-light";
  return (
    <div className={`card ${themeClass}`}>
      <div className="card-body">{children}</div>
    </div>
  );
};
