import styled from "styled-components";
import { flexFn, marginFn } from "../../styles/common.styled";

export const HomePageStyled = styled.div`
  ${flexFn()}
  ${marginFn()}
  height: 100%;
  gap: 20px;
  flex-wrap: wrap;

  > * {
    min-width: 20rem;
  }
`;
