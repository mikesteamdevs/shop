import { IProduct } from "../interfaces/product.interface";
import products from "../data/products.json";

type ProductsResponse = IProduct[];

export class ProductsService {
  public static getAllProducts = (): Promise<ProductsResponse> => {
    return new Promise((resolve, _reject) => resolve(products));
  };

  public static getProductsInBasket = (): Promise<ProductsResponse> => {
    return new Promise((resolve, _reject) => resolve(products.slice(0, 5)));
  };
}
