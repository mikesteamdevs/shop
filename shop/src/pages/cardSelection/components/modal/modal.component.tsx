import React, { useMemo, useState } from "react";
import { ICreditCard } from "../../../../interfaces/creditCard.interface";
import { ModalForm } from "./modal.form";
import { ModalFormWrapperStyled } from "./modal.styled";

interface IModalProps {
  onClose: () => void;
  onSave: (card: ICreditCard) => void;
}

export interface IValidation {
  isValid: boolean;
}

interface IFormField<T> extends IValidation {
  value: T;
}
export interface IFormFields {
  cvv: IFormField<string>;
  cardNumber: IFormField<string>;
  expiredDate: IFormField<string>;
  cardHolderName: IFormField<string>;
}

export const Modal: React.FC<IModalProps> = ({ onClose, onSave }) => {
  const [fields, setFields] = useState<IFormFields>({
    cvv: {
      isValid: false,
      value: "",
    },
    cardHolderName: {
      isValid: false,
      value: "",
    },
    cardNumber: {
      isValid: false,
      value: "",
    },
    expiredDate: {
      isValid: false,
      value: "",
    },
  });

  const onChange = (fieldName: keyof IFormFields, value: string) => {
    const newFields = { ...fields };
    newFields[fieldName].value = value;
    setFields(validateFields(fieldName, newFields));
  };

  const validateFields = (
    fieldName: keyof IFormFields,
    fieldsToValidate: IFormFields
  ) => {
    const newFields = { ...fieldsToValidate };
    let isValid = false;
    const value = newFields[fieldName].value;
    switch (fieldName) {
      case "cvv":
        isValid = new RegExp(/^[0-9]{3,4}$/).test(value);
        break;
      case "cardNumber":
        isValid = new RegExp(
          /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/
        ).test(value);
        break;
      case "cardHolderName":
        isValid = new RegExp(/^((?:[A-Za-z]+ ?){1,3})$/).test(value);
        break;
      case "expiredDate":
        const regex = new RegExp(/^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/);
        isValid = regex.test(value);
        break;
    }

    newFields[fieldName].isValid = isValid;
    return newFields;
  };

  const areAllFieldsValid = useMemo(() => {
    const areValid = !Object.keys(fields).some(
      (key) => !fields[key as keyof IFormFields].isValid
    );
    return areValid;
  }, [fields]);

  const onAddNewCard = () => {
    const { cardHolderName, cardNumber, cvv, expiredDate } = fields;
    const newCard: ICreditCard = {
      id: `card-${new Date().getTime().toString()}`,
      cardHolderName: cardHolderName.value,
      cardNumber: cardNumber.value,
      cvv: cvv.value,
      expirationDate: expiredDate.value,
    };
    onSave(newCard);
  };

  return (
    <ModalFormWrapperStyled>
      <ModalForm
        fields={fields}
        areFieldsValid={areAllFieldsValid}
        onSave={onAddNewCard}
        onClose={onClose}
        onChange={onChange}
      />
    </ModalFormWrapperStyled>
  );
};
