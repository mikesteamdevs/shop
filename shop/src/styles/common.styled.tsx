import { css } from "styled-components";

type JustifyContentType =
  | "flex-start"
  | "flex-end"
  | "center"
  | "space-between"
  | "space-around"
  | "space-evenly";

type AlignItemsType = "center" | "start" | "end" | "stretch";
type FlexDirectionType = "row" | "column";
export const flexFn = (
  justifyContent: JustifyContentType = "center",
  alignItems: AlignItemsType = "center",
  flexDirection: FlexDirectionType = "row"
) => {
  return css`
    display: flex;
    justify-content: ${justifyContent};
    align-items: ${alignItems};
    flex-direction: ${flexDirection};
  `;
};

type MarginType = "small" | "medium" | "large";

const getMarginFromType = (marginType: MarginType) => {
  switch (marginType) {
    case "large":
      return "4rem";
    case "medium":
      return "2rem";
    default:
      return "1rem";
  }
};

export const marginFn = (marginType: MarginType = "small") => {
  return css`
    margin: ${getMarginFromType(marginType)};
  `;
};
