import React from "react";
import { ICreditCard } from "../../../../interfaces/creditCard.interface";
import { SliderItem } from "./sliderItem.compnent";
import { SliderItemWrapperStyled } from "./sliderItem.styled";

interface ISliderProps {
  activeCardId: string;
  cards: ICreditCard[];
  onNext: () => void;
  onPrev: () => void;
}

export const Slider: React.FC<ISliderProps> = ({
  cards,
  activeCardId,
  onNext,
  onPrev,
}) => {
  return (
    <div
      id="carouselExampleControls"
      className="carousel slide"
      data-ride="carousel"
    >
      <div className="carousel-inner">
        <SliderItemWrapperStyled>
          {cards.map((card) => (
            <SliderItem
              key={card.id}
              card={card}
              isActive={activeCardId === card.id}
            />
          ))}
        </SliderItemWrapperStyled>
      </div>
      <div
        className="carousel-control-prev"
        role="button"
        data-slide="prev"
        onClick={onPrev}
      >
        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
        <span className="sr-only">Previous</span>
      </div>
      <div
        className="carousel-control-next"
        role="button"
        data-slide="next"
        onClick={onNext}
      >
        <span className="carousel-control-next-icon" aria-hidden="true"></span>
        <span className="sr-only">Next</span>
      </div>
    </div>
  );
};
