import React from "react";
import { ButtonRightStyled, TwoButtonsStyled } from "./buttonsSection.styled";

interface IButtonsSectionProps {
  nextComponent: React.ReactElement;
  backComponent?: React.ReactElement;
}

export const ButtonsSection: React.FC<IButtonsSectionProps> = ({
  backComponent,
  nextComponent,
}) => {
  if (!backComponent)
    return <ButtonRightStyled>{nextComponent}</ButtonRightStyled>;
  return (
    <TwoButtonsStyled>
      <>{backComponent}</>
      <>{nextComponent}</>
    </TwoButtonsStyled>
  );
};
