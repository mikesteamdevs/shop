import { IProduct } from "../interfaces/product.interface";

export const sumProducts = (products: IProduct[]): number => {
    return products
      ?.map(({ price }) => price)
      .reduce((prev, next) => prev + next, 0);
}
  
export const getMaker = (cardNumber: string) => {
    const firstNumber = cardNumber.split("")[0];
    switch (firstNumber) {
      case "4":
        return "Visa";
      case "5":
        return "Master Card";
      default:
        return "Other maker";
    }
  };