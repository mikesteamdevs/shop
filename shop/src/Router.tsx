import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Basket } from "./pages/basket/basket.page";
import { Header } from "./components/header/header.component";
import { Home } from "./pages/home/home.page";
import { CardSelection } from "./pages/cardSelection/cardSelection.page";
import { TransactionConfirmation } from "./pages/transactionConfirmation/transactionConfirmation.page";
import { NotFound } from "./pages/notFound/notFound.page";
import { useSelector } from "react-redux";
import { numberOfProductsInTheBasket } from "./redux/reducer";

export function Router() {
  const numberOfProducts = useSelector(numberOfProductsInTheBasket);
  return (
    <BrowserRouter>
      <Header numberOfItems={numberOfProducts} />
      <main>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/basket">
            <Basket />
          </Route>
          <Route exact path="/select-card">
            <CardSelection />
          </Route>
          <Route exact path="/transaction-confirmation">
            <TransactionConfirmation />
          </Route>
          <Route path="/">
            <NotFound />
          </Route>
        </Switch>
      </main>
    </BrowserRouter>
  );
}
