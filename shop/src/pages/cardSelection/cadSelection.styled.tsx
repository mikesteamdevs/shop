import styled from "styled-components";
import { marginFn } from "../../styles/common.styled";

export const CardSelectionPageStyled = styled.div`
  display: grid;
  grid-template-rows: auto auto;
  > * {
    height: 100%;
    ${marginFn()}
  }
`;
