import React from "react";
import { Link } from "react-router-dom";

interface IHeaderProps {
  numberOfItems: number;
}

export const Header: React.FC<IHeaderProps> = ({ numberOfItems }) => {
  return (
    <nav className="navbar u-flex-space-between-center navbar-dark bg-primary">
      <a className="navbar-brand" href="/">
        SHOP
      </a>
      <Link to="/basket">
        <button className="btn btn-secondary">
          <i className="bi-cart-check text-white" /> {numberOfItems}
        </button>
      </Link>
    </nav>
  );
};
