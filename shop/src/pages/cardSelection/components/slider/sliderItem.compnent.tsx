import React from "react";
import { ICreditCard } from "../../../../interfaces/creditCard.interface";
import { CreditCard } from "../creditCard/creditCard.component";

interface ISliderItemProps {
  card: ICreditCard;
  isActive: boolean;
}

export const SliderItem: React.FC<ISliderItemProps> = ({ isActive, card }) => {
  return (
    <div>
      <div className={`carousel-item ${isActive && "active"}`}>
        <CreditCard card={card} />
      </div>
    </div>
  );
};
