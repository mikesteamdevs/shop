import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card } from "../../components/card/card.component";
import {
  productsInTheBasket,
  selectedCreditCard,
  clearNumberOfItemsInBasket,
} from "../../redux/reducer";
import { getMaker, sumProducts } from "../../utils/utils";
import {
  CreditCardInfoStyled,
  SuccessTitleStyled,
  TransactionConfirmationPageStyled,
} from "./transactionConfirmation.styled";

export const TransactionConfirmation = () => {
  const dispatch = useDispatch();
  const products = useSelector(productsInTheBasket);
  const creditCard = useSelector(selectedCreditCard);
  const sum = useMemo((): number => sumProducts(products), [products]);

  useEffect(() => {
    dispatch(clearNumberOfItemsInBasket());
  }, [dispatch]);

  const maker = creditCard?.cardNumber
    ? getMaker(creditCard?.cardNumber)
    : "No card found";

  return (
    <TransactionConfirmationPageStyled>
      <SuccessTitleStyled>
        <i className="bi-check" />
        <h4>Transaction was successful.</h4>
      </SuccessTitleStyled>
      <div>
        {products.map(({ name, price }) => (
          <Card key={name}>
            <div className="u-flex-space-between-center">
              <span>{name}</span>
              <span>{price} PLN</span>
            </div>
          </Card>
        ))}
        <Card theme="dark">
          <div className="u-flex-space-between-center">
            <span>Sum</span>
            <span>{sum} PLN</span>
          </div>
        </Card>
      </div>
      <CreditCardInfoStyled>
        <Card theme="dark">
          <div className="u-flex-space-between-center">
            <span>Payed by {maker}</span>
            <span>{creditCard?.cardNumber}</span>
          </div>
        </Card>
      </CreditCardInfoStyled>
    </TransactionConfirmationPageStyled>
  );
};
