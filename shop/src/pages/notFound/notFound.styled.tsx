import styled from "styled-components";
import { flexFn, marginFn } from "../../styles/common.styled";

export const NotFoundStyled = styled.div`
  ${flexFn()}
  ${marginFn()}
  height: inherit;
`;
