import styled from "styled-components";
import { flexFn, marginFn } from "../../styles/common.styled";

export const ButtonRightStyled = styled.div`
  ${flexFn("flex-end", "center")}
  ${marginFn()}
`;
export const TwoButtonsStyled = styled.div`
  ${flexFn("space-between", "center")}
  ${marginFn()}
`;
