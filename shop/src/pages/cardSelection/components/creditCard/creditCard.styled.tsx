import styled, { css } from "styled-components";
import { COLORS } from "../../../../styles/colors";
import { flexFn } from "../../../../styles/common.styled";

type CardThemeType = "Visa" | "Master Card" | "Other maker";

interface ICreditCardStyled {
  theme: CardThemeType;
}

const getGradient = (theme: CardThemeType) => {
  switch (theme) {
    case "Visa":
      return css`
        background: linear-gradient(
          90deg,
          rgba(2, 0, 36, 1) 0%,
          rgba(9, 9, 121, 1) 35%,
          rgba(0, 212, 255, 1) 100%
        );
      `;
    case "Master Card":
      return css`
        background: linear-gradient(
          30deg,
          rgba(131, 58, 180, 1) 0%,
          rgba(253, 29, 29, 1) 50%,
          rgba(252, 176, 69, 1) 100%
        );
      `;
    default:
      return css`
        background: linear-gradient(
          90deg,
          rgba(2, 0, 36, 1) 0%,
          #b8c417 35%,
          #35bd62 100%
        );
      `;
  }
};

export const CreditCardStyled = styled.div<ICreditCardStyled>`
  width: 27rem;
  height: 15rem;
  ${({ theme }) => getGradient(theme)}
  color: ${COLORS.white};
  border-radius: 1rem;
  padding: 1rem 1rem 1rem 4rem;
`;

export const LogoStyled = styled.div`
  ${flexFn("flex-end")}
`;

export const CardItemStyled = styled.div`
  ${flexFn("flex-start")}
`;

export const CardNumberItemStyled = styled.div`
  font-size: 1.5rem;
`;

export const DateCVVStyled = styled.div`
  ${flexFn()}
  > * {
    width: 100%;
  }
`;
