import React from "react";
import { NotFoundStyled } from "./notFound.styled";

export const NotFound = () => {
  return <NotFoundStyled>Not found</NotFoundStyled>;
};
